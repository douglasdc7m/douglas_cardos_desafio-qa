/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katachallangeapp;

/**
 *
 * @author Douglas
 */
public class Product {
    private double unitPrice;
    private double bundlePrice;
    private int bundle;
    private int quantity=0;
    private double myPrice;
    private String name;
    
    public double myPrice() {
        if ((quantity % bundle) == 0)
        {
            myPrice = (quantity/bundle)*bundlePrice;
        }
        else
        {
            myPrice = quantity*unitPrice;
        }
        
        System.out.println("The subtotal price is: " + myPrice);
        return myPrice;
    }  
    
    public void setUnitPrice(int uPrice){
        unitPrice = uPrice;
    }
        
    public double getUnitPrice(){
        return unitPrice;
    }
    
    public void setBundlePrice(int bPrice){
        bundlePrice = bPrice;
    }
        
    public double getBundlePrice(){
        return bundlePrice;
    }

    public void setBundle(int bundle){
        this.bundle = bundle;
    }
        
    public int getBundle(){
        return bundle;
    }
    
    public void setName(String name){
        this.name = name;
    }
        
    public String getName(){
        return name;
    }    
   
    public int toBuy(int numItens){
        this.quantity = this.quantity + numItens;
        System.out.println("You choose: " + this.quantity + " itens(s)");
        return this.quantity;
    }     
    
}
