/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katachallangeapp;

import katachallangeapp.Product;

/**
 *
 * @author Douglas
 */
public class ProductTestDrive {
    public static void main (String[] args) {
        double total;
        //Teste code
        Product A = new Product();
        A.setName("ProductA"); 
        A.setUnitPrice(1);  
        A.setBundle(3); 
        A.setBundlePrice(2); //special price
        System.out.println("Name: " + A.getName());
        System.out.println("Unit price: " + A.getUnitPrice());
        System.out.println("Bundle: " + A.getBundle());
        System.out.println("Bundle Price: " + A.getBundlePrice()); 
        A.myPrice(); //0.0
        A.toBuy(2);
        A.toBuy(3);
        A.myPrice(); //5.0
        A.toBuy(1); //6 itens
        A.myPrice(); //4.0
        
        Product B = new Product();
        B.setName("ProductB"); 
        B.setUnitPrice(1);  
        B.setBundle(5);
        B.setBundlePrice(3);
        System.out.println("Name: " + B.getName());
        System.out.println("Unit price: " + B.getUnitPrice());
        System.out.println("Bundle: " + B.getBundle());
        System.out.println("Bundle Price: " + B.getBundlePrice()); 
        B.myPrice(); //0.0
        B.toBuy(2);
        B.toBuy(3); //5 itens
        B.myPrice(); //3.0
        B.toBuy(1); //6 itens
        B.myPrice();  //4.0   

        total = A.myPrice() + B.myPrice();
        System.out.println("Total price: " + total); //10.0
        
    }   
}
