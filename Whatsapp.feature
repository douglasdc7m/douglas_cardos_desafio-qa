Feature: Send message
  A user can send messages to a contact

  Scenario Outline: a user select a contact to send a message
    Given I should be at my Whatsapp contact list
    When I select <contact> from my list
    Then A message page oppens
    When I write a <message>
    And I click on Send button
    Then The <message> is sent to my <contact>

  Examples: 
    | contact | Message |
    |  "jonhDoe1"   |  "message1"  |
    |  "jonhDoe2"   |  "message2"  |
    |  "jonhDoe3"   |  "message3"  |

  Scenario Outline: a user deletes a chat from list
    Given I should be at my Whatsapp chat list
    When I hang over a <chat> from my list
    Then A menu bar is shown over my chat list
    And The <chat> is marked in green
    When I click on Trash button
    And I confirm the exclusion of <chat>
    Then The <chat> is deleted from my chat list

  Examples: 
    | chat |
    |  "jonhDoe1"   |
    |  "jonhDoe2"   |
    |  "jonhDoe3"   |
